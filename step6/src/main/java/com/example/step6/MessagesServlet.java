package com.example.step6;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

public class MessagesServlet extends HttpServlet {
    TemplateEngine templateEngine;

    DefaultMessageService defaultMessageService;

    public MessagesServlet(TemplateEngine templateEngine,DefaultMessageService defaultMessageService) {
        this.templateEngine = templateEngine;
        this.defaultMessageService = defaultMessageService;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

      //  templateEngine.render("chat.html",resp);

        Map<String, Object> params = Map.of(
                //"profiles", List.of(new Profile("Tim","Cook","https://scontent.fiev9-1.fna.fbcdn.net/v/t1.6435-9/100063325_3065270396867687_1369217968893853696_n.jpg?_nc_cat=109&ccb=1-7&_nc_sid=8bfeb9&_nc_ohc=HtIe2ueOHTgAX9WBur-&_nc_ht=scontent.fiev9-1.fna&oh=00_AfDSqfrydSyDOpcbnMqwwnoc2YZ8vbZcn43BMqU2OxcFJg&oe=64096BD8"))
                "messages", defaultMessageService.findByProfile(req.getParameter("id"))
        );

        templateEngine.render("chat2.ftl",params,resp);
    }
}
