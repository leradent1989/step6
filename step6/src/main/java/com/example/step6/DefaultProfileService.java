package com.example.step6;
import java.util.List;

public class DefaultProfileService {
    JdbcProfileDao jdbcProfileDao;


    public DefaultProfileService(JdbcProfileDao jdbcProfileDao) {
        this.jdbcProfileDao = jdbcProfileDao;
    }

    public List <Profile> findAll(){

        return  jdbcProfileDao.findAll();
    }
    public  Profile findByIndex(int index){
        return  jdbcProfileDao.findByIndex(index);
    }
}
