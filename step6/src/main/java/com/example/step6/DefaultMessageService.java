package com.example.step6;

import java.util.List;

public class DefaultMessageService {
    JdbcMessageDao jdbcMessageDao;

    public DefaultMessageService(JdbcMessageDao jdbcMessageDao) {
        this.jdbcMessageDao = jdbcMessageDao;
    }
    public List <Message> findAll(){
        return  jdbcMessageDao.findAll();
    }
    public List <Message> findByProfile(String id){
        return  jdbcMessageDao.findByProfile(id);
    }
}
