package com.example.step6;

public class Profile {
    private String id;
    private String name;
    private String surname;
    private String imgUrl;




    public Profile(String id,String name,String surname,String imgUrl) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.imgUrl = imgUrl;

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    @Override
    public String toString() {
        return "Profile{" +
                "id='" + id + '\'' +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", imgUrl='" + imgUrl + '\'' +
                '}';
    }
    public String getId(Profile profile){
        return  id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
