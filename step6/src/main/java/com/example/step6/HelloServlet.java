package com.example.step6;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.ServletContext;
import java.io.IOException;
import com.example.step6.Profile;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import javax.servlet.RequestDispatcher;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


public class HelloServlet extends HttpServlet {
    public TemplateEngine templateEngine;
    //LikedDao likedDao ;

    public  DefaultProfileService defaultProfileService;

    DefaultLikedService defaultLikedService;

    public HelloServlet(TemplateEngine templateEngine,DefaultLikedService defaultLikedService,DefaultProfileService defaultProfileService) {
        this.templateEngine = templateEngine;
        this.defaultLikedService =defaultLikedService;
        this.defaultProfileService = defaultProfileService;
    }
    //public List  <Profile> profileList = List.of(new Profile("1","Tim","Cook","https://scontent.fiev9-1.fna.fbcdn.net/v/t1.6435-9/100063325_3065270396867687_1369217968893853696_n.jpg?_nc_cat=109&ccb=1-7&_nc_sid=8bfeb9&_nc_ohc=HtIe2ueOHTgAX9WBur-&_nc_ht=scontent.fiev9-1.fna&oh=00_AfDSqfrydSyDOpcbnMqwwnoc2YZ8vbZcn43BMqU2OxcFJg&oe=64096BD8"),new Profile("2","Gordon","Freeman","https://i.pinimg.com/736x/6a/97/50/6a975048e4b0e5a5f9c497af10551c48.jpg"),new Profile("3","Paul","Newman","https://scontent.fiev9-1.fna.fbcdn.net/v/t1.6435-9/100063325_3065270396867687_1369217968893853696_n.jpg?_nc_cat=109&ccb=1-7&_nc_sid=8bfeb9&_nc_ohc=HtIe2ueOHTgAX9WBur-&_nc_ht=scontent.fiev9-1.fna&oh=00_AfDSqfrydSyDOpcbnMqwwnoc2YZ8vbZcn43BMqU2OxcFJg&oe=64096BD8"));
    int count = 0;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");

        resp.getWriter().println("""
           
            <h2>Hello from servlet</h2>
            
        """);
        count=0;
        Map<String, Object> params = Map.of(
                //"profiles", List.of(new Profile("Tim","Cook","https://scontent.fiev9-1.fna.fbcdn.net/v/t1.6435-9/100063325_3065270396867687_1369217968893853696_n.jpg?_nc_cat=109&ccb=1-7&_nc_sid=8bfeb9&_nc_ohc=HtIe2ueOHTgAX9WBur-&_nc_ht=scontent.fiev9-1.fna&oh=00_AfDSqfrydSyDOpcbnMqwwnoc2YZ8vbZcn43BMqU2OxcFJg&oe=64096BD8"))
                "profiles", List.of(defaultProfileService.findByIndex(count))
        );

        templateEngine.render("profile.ftl",params,resp);


    }
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String param1 = req.getParameter("param1");
        System.out.println(param1);
        String param2 = req.getParameter("param2");
        System.out.println(param2);
        if(param1 == null){
            param1 ="No";
        }
        if(param1.equals("YES")){
         defaultLikedService.addToList(defaultProfileService.findByIndex(count));
        }
     if(count >= defaultProfileService.findAll().size() -1){
         count=0;
         String path = req.getContextPath() + "/liked";
         resp.sendRedirect(path);


     }
         Map<String, Object> params = Map.of(
                 "profiles", List.of(defaultProfileService.findByIndex(++count))
         );
         templateEngine.render("profile.ftl", params, resp);

    }
}
