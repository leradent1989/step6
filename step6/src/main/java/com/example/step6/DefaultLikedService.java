package com.example.step6;

import java.util.List;

public class DefaultLikedService {
   private LikedDao likedDao;

    public DefaultLikedService(LikedDao likedDao) {
        this.likedDao = likedDao;
    }

    public List<Profile> findAll(){
        return  likedDao.findAll();
    }

    public  void addToList(Profile profile){
        likedDao.addToList(profile);
    }
}
