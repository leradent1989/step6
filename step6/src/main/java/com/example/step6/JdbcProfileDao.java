package com.example.step6;

import java.util.List;

public class JdbcProfileDao {
    List <Profile> profileList;


    public JdbcProfileDao(List<Profile> profileList) {
        this.profileList = profileList;
    }

    public List <Profile> findAll(){
        return  profileList;
    }
    public Profile  findByIndex(int index){
        return  profileList.get(index);
    }
}
