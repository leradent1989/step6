package com.example.step6;
import  java.util.List;
import java.util.stream.Collectors;

public class JdbcMessageDao {
    List <Message> messageList;

    public JdbcMessageDao(List<Message> messageList) {
        this.messageList = messageList;
    }


    public  List <Message> findAll (){
        return  messageList;
    }
    public  List <Message> findByProfile (String id){
       return    messageList.stream().filter(el -> el.getProfile().getId().equals(id)).collect(Collectors.toList());
    }
}
