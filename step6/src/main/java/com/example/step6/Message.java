package com.example.step6;

public class Message {

    Profile profile;
    String text;

    public Message(Profile profile,String text) {
        this.profile = profile;
        this.text = text;
    }


    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
