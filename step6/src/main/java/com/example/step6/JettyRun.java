package com.example.step6;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;

import java.util.ArrayList;
import java.util.List;


public class JettyRun  {
    public static void main(String[] args) throws Exception {
        Server server = new Server(8081);
        TemplateEngine templateEngine = new TemplateEngine();
        ServletContextHandler handler = new  ServletContextHandler();
        List<Profile> profileList = List.of(new Profile("1","Tim","Cook","https://scontent.fiev9-1.fna.fbcdn.net/v/t1.6435-9/100063325_3065270396867687_1369217968893853696_n.jpg?_nc_cat=109&ccb=1-7&_nc_sid=8bfeb9&_nc_ohc=HtIe2ueOHTgAX9WBur-&_nc_ht=scontent.fiev9-1.fna&oh=00_AfDSqfrydSyDOpcbnMqwwnoc2YZ8vbZcn43BMqU2OxcFJg&oe=64096BD8"),new Profile("2","Gordon","Freeman","https://i.pinimg.com/736x/6a/97/50/6a975048e4b0e5a5f9c497af10551c48.jpg"),new Profile("3","Paul","Newman","https://scontent.fiev9-1.fna.fbcdn.net/v/t1.6435-9/100063325_3065270396867687_1369217968893853696_n.jpg?_nc_cat=109&ccb=1-7&_nc_sid=8bfeb9&_nc_ohc=HtIe2ueOHTgAX9WBur-&_nc_ht=scontent.fiev9-1.fna&oh=00_AfDSqfrydSyDOpcbnMqwwnoc2YZ8vbZcn43BMqU2OxcFJg&oe=64096BD8"));
         List<Message> messageList = List.of(new Message(profileList.get(0),"hello gfg"),new Message(profileList.get(1),"hello 2"),new Message(profileList.get(2),"hello 3"));

        JdbcMessageDao jdbcMessageDao = new JdbcMessageDao(messageList);
        DefaultMessageService defaultMessageService = new DefaultMessageService(jdbcMessageDao);
        LikedDao likedDao = new LikedDao(new ArrayList<>());
        JdbcProfileDao jdbcProfileDao = new JdbcProfileDao(profileList);
        DefaultProfileService defaultProfileService = new DefaultProfileService(jdbcProfileDao);
        DefaultLikedService defaultLikedService = new DefaultLikedService(likedDao);
        HelloServlet helloServlet = new HelloServlet(templateEngine,defaultLikedService,defaultProfileService);
        LikedServlet likedServlet = new LikedServlet(templateEngine,defaultLikedService);
        MessagesServlet messagesServlet = new MessagesServlet(templateEngine,defaultMessageService);
        LoginServlet loginServlet = new LoginServlet(templateEngine);
        handler.addServlet( new ServletHolder(helloServlet),"/users");
        handler.addServlet( new ServletHolder(likedServlet),"/liked");
        handler.addServlet(new ServletHolder(messagesServlet),"/messages" );
        handler.addServlet( new ServletHolder(loginServlet),"/");


       server.setHandler(handler);
       server.start();
       server.join();
    }
}
